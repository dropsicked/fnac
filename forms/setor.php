<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php
	$empid = $_GET["cd"];
 	//error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	include "../php/connect.php";
	$sql1 = "SELECT * From tbsuperintendencia WHERE cdEmpresa = " . $empid;
	$res1 = mysqli_query($link,$sql1);
	$sql2 = "SELECT * From tbgerencia WHERE cdEmpresa = " . $empid;
	$res2 = mysqli_query($link,$sql2);
	$sql3 = "SELECT * From tbsetor WHERE cdEmpresa = " . $empid;
	$res3 = mysqli_query($link,$sql3);
	header ('Content-type: text/html; charset=UTF-8');
?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/divcontrato.php?cd=<?php echo $empid; ?>&type=1" href="javascript:;" class="novobotao text-center" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Nova Superintendência</b></div>
			</div>
			<div class="col-md-4">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/divcontrato.php?cd=<?php echo $empid; ?>&type=2" href="javascript:;" class="novobotao text-center" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Novo Gerência</b></div>
			</div>
			<div class="col-md-4">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/divcontrato.php?cd=<?php echo $empid; ?>&type=3" href="javascript:;" class="novobotao text-center" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Novo Setor</b></div>
			</div>
		</div>
		<div class="row">
			<div id="tabela" class="col-md-4">
					<table class="table table-striped" border="2" style="margin-top: 40px">
						<thead class="thead-dark">
							<tr>
								<th>Gerência Geral</th>
								<th>Modificar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($res1->num_rows > 0){
									while($row = mysqli_fetch_assoc($res1)){
										echo '
											<tr>
												<td><b>' . $row["superintendencia"] . '</b></td>
												<td><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/edit_setor.php?cd=' . $row["cdSuperIntendencia"] . '?type=1" href="javascript:;"id="modcadastro" class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/edit.png"/></b></td>
												<td><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/delete.png"/></b></td>
											</tr>
										';
									}
								}else{
									echo ' <td class="text-center" colspan="5">Não há nenhuma superintedência cadastrado</td> ';
								}
							?>
						<tbody>
					</table>
			</div>
			<div id="tabela" class="col-md-4">
					<table class="table table-striped" border="2" style="margin-top: 40px">
						<thead class="thead-dark">
							<tr>
								<th>Gerência Geral</th>
								<th>Gerência</th>
								<th>Modificar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($res2->num_rows > 0){
									while($row2 = mysqli_fetch_assoc($res2)){
										$cdSI = $row2["cdSuperIntendencia"];
										$sql22 = "SELECT * FROM tbsuperintendencia WHERE cdEmpresa = " . $empid . " AND cdSuperIntendencia = " . $cdSI ;
										$res22 = mysqli_query($link,$sql22);
										while($row22 = mysqli_fetch_assoc($res22)){
											echo '
												<tr>
													<td><b>' . $row22["superintendencia"] . '</b></td>
													<td><b>' . $row2["gerencia"] . '</b></td>
													<td><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/edit_setor.php?cd=' . $row2["cdGerencia"] . '?type=2" href="javascript:;"id="modcadastro" class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/edit.png"/></b></td>
													<td><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/delete.png"/></b></td>
												</tr>
											';
										}
									}
								}else{
									echo ' <td class="text-center" colspan="5">Não há nenhuma Gerência cadastrado</td> ';
								}
							?>
						<tbody>
					</table>
			</div>
			<div id="tabela" class="col-md-4">
					<table class="table table-striped" border="2" style="margin-top: 40px">
						<thead class="thead-dark">
							<tr>
								<th>Gerência Geral</th>
								<th>Gerência</th>
								<th>Setor</th>
								<th>Modificar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($res3->num_rows > 0){
									while($row3 = mysqli_fetch_assoc($res3)){
										$cdGerencia = $row3["cdGerencia"];
										$sql5 = "SELECT * From tbgerencia WHERE cdEmpresa = " . $empid . " AND cdGerencia = " . $cdGerencia;
										$res5 = mysqli_query($link,$sql5);
										while($row5 = mysqli_fetch_assoc($res5)){
											$sql6 = "SELECT * From tbsuperintendencia WHERE cdEmpresa = " . $empid . " AND cdSuperIntendencia = ". $row5["cdSuperIntendencia"];
											$res6 = mysqli_query($link,$sql6);
											while($row6 = mysqli_fetch_assoc($res6)){
												echo '
													<tr>
														<td><b>' . $row6["superintendencia"] . '</b></td>
														<td><b>' . $row5["gerencia"] . '</b></td>
														<td><b>' . $row3["setor"] . '</b></td>
														<td><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/edit_setor.php?cd=' . $row3["cdSetor"] . '?type=3" href="javascript:;"id="modcadastro" class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/edit.png"/></b></td>
														<td><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/delete.png"/></b></td>
													</tr>
												';
											}
										}
									}
								}else{
									echo ' <td class="text-center" colspan="5">Não há nenhum setor cadastrado</td> ';
								}
							?>
						<tbody>
					</table>
			</div>
		</div>
	</div>
</html>