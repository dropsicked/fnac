<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php 
 	error_reporting(E_ALL ^ E_DEPRECATED);
	include "../php/connect.php";
	$empid = $_SESSION["cdempresa"];	
	$sql1 = "SELECT * From tbempresa WHERE cdEmpresa = ". $empid;
	$res1 = mysqli_query($connect,$sql1);
	header ('Content-type: text/html; charset=UTF-8');
?>
	<div class="container-fluid">
		<div class="row"><div class="col-12"><h3>Empresas</h3></div></div>
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8"></div>
			<div class="col-2">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/empresa.php" href="javascript:;" class="novobotao" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Nova Empresa</b></div>
			</div>
		</div>
		<div class="row">
			<div id="tabela" class="col-12">
					<table class="table table-striped table-responsive-xl table-sm" border="2" style="margin-top: 40px">
						<thead class="thead-dark">
							<tr>
								<th>Cadastro</th>
								<th>Logo</th>
								<th>Nome da Empresa</th>
								<th>Endereço</th>
								<th>Razão Social</th>
								<th>Nome Fantasia</th>
								<th>CNPJ</th>
								<th>Contratos</th>
								<th>Setores</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($res1->num_rows > 0){
									while($row = mysqli_fetch_assoc($res1)){
										echo '
											<tr style="background-color: ">
												<td>' . $row["cdEmpresa"] . '</td>
												<td><img width="50px" src="img_empresas/' . $row["arquivo"] . '" /></b></td>
												<td><b>' . $row["nomeEmpresa"] . '</b></td>
												<td><b>' . $row["endereco"] . '</b></td>
												<td><b>' . $row["razaoSocial"] . '</b></td>
												<td><b>' . $row["nomeFantasia"] . '</b></td>
												<td><b>' . $row["cnpj"] .  '</b></td>
												<td><b><img data-fancybox data-type="ajax" data-src="forms/contrato.php?cd=' . $row["cdEmpresa"] . '" href="javascript:;" class="icone2 contrato" style="cursor: pointer" width="24px" height="24px" src="img/icons/document.png"/></b></td>
												<td><b><img data-fancybox data-type="ajax"	data-src="forms/setor.php?cd='. $row["cdEmpresa"].'" href="javascript:;"class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/divsetor.png"/></b></td>
												<td><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/edit/edit_empresa.php?cd=' . $row["cdEmpresa"] . '" href="javascript:;" class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/edit.png"/></b></td>
												<td><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/post/form_delEmpresa.php?cd=' . $row["cdEmpresa"] . '" href="javascript:; class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/delete.png"/></b></td>
											</tr>
										';
									}
								}else{
									echo '
										<tr style="background-color: ">
											<td colspan="11">Nehnuma Empresa Cadastrada</td>
										</tr>
									';
								}
							?>
						<tbody>
					</table>
			</div>
		</div>
	</div>
</html>