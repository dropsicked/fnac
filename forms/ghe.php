<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php 
 	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	include "../php/connect.php";
	session_start();
	$empid = $_SESSION["cdempresa"];
	$contrato = $_SESSION["cdcontrato"];
	$sql1 = "SELECT * From tbghe WHERE codContrato = ".$_SESSION["cdcontrato"];
	$res1 = mysqli_query($connect,$sql1);
	header ('Content-type: text/html; charset=UTF-8');
?>
	<div class="container-fluid">
		<div class="row"><div class="col-12"><h3>GHE</h3></div></div>
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8"></div>
			<div class="col-2">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/ghe.php?cd=<?php echo $empid;?>&cid=<?php echo $contrato;?>" href="javascript:;" class="novobotao" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Nova GHE</b></div>
			</div>
		</div>
		<div class="row">
			<div id="tabela" class="col-md-12">
				<table class="table table-striped table-responsive-xl table-sm" border="2" style="margin-top: 40px">
					<thead class="thead-dark">
						<tr>
							<th>Código</th>							
							<th>Código Único de GHE</th>
							<th>Contrato</th>
							<th>Nome</th>
							<th>Número de Empregados</th>
							<th>Jornada de Trabalho</th>
							<th>Descrição do Trabalho</th>
							<th>Tipo de GHE</th>
							<th>Descrição da atividade</th>
							<th>Cargos da GHE</th>
							<th>Modificar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($res1->num_rows > 0){
								while($row = mysqli_fetch_assoc($res1)){
									$sql2 = "SELECT * From tbcontrato WHERE cdContrato = " . $row["cdContrato"];
									$res2 = mysqli_query($connect,$sql2);
									if($row["tipoCont"] == 1){
										$tipoGHE = "LTCAT";
									}else{
										if($row["tipoCont"] == 2){
											$tipoGHE = "PPRA Quantitativo";
										}else{
											if($row["tipoCont"] == 3){
												$tipoGHE = "PPRA Inserção Direta";
											}else{
												$tipoGHE = "PPRA Qualitativo";
											}
										}
									}
									while($row2 = mysqli_fetch_assoc($res2)){
										$sql3 = "SELECT * From tbempresa WHERE cdContrato = " . $row2["cdEmpresa"];
										$res3 = mysqli_query($connect,$sql3);
										while($row3 = mysqli_fetch_assoc($res3)){
											$contrato = $row3["nomeEmpresa"] . $row2["cdContrato"];
											echo '
												<tr style="background-color: ">
													<td><b>' . $row["cdGHE"] . '</b></td>
													<td><b>' . $row["codGHE"] . '</b></td>
													<td><b>' . $contrato . '</b></td>
													<td><b>' . $row["nomeGHE"] . '</b></td>
													<td><b>' . $row["numEmpregados"] . '</b></td>
													<td><b>' . $row["jornadatrab"] . '</b></td>
													<td><b><img src="img/icons/info.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/info/ghe_desc_trab.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
													<td><b>' . $tipoGHE .  '</b></td>
													<td><b><img src="img/icons/info.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/info/ghe_desc_ativ.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
													<td><b>' . $row["cargosGHE"] . '</b></td>
													<td><b><img src="img/icons/edit.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/cadastro/edit/ghe.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
													<td><b><img src="img/icons/delete.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/cadastro/post/forms_delGHE.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
												</tr>
											';
										}
									}
								}
							}else{
								echo '
									<tr style="background-color: ">
										<td colspan="12">Nehnuma GHE Cadastrada</td>
									</tr>
								';
							}
						?>
					<tbody>
				</table>
			</div>
		</div>
	</div>
</html>