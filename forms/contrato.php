<html lang="en">
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fnac.css">
		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
<?php
	$empid = $_GET["cd"];
	include "../php/connect.php";
	$sql4 = "SELECT cdEmpresa,nomeEmpresa FROM tbempresa WHERE cdEmpresa = " . $empid;
	$res4 = mysqli_query($link,$sql4);	
	$sql3 = "SELECT cdEmpresa,nomeEmpresa FROM tbempresa WHERE cdEmpresa = " . $empid;
	$res3 = mysqli_query($link,$sql3);
	$sql2 = "SELECT * From tbcontrato WHERE cdEmpresa = " . $empid;
	$res2 = mysqli_query($link,$sql2);
?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<h4>Contratos da Empresa : <?php
					while ($resul3 = mysqli_fetch_assoc($res3)){
						echo $resul3["nomeEmpresa"];
					}
					?>
				</h4>
			</div>
			<div class="col-md-4">
				<div data-fancybox data-type="ajax" data-src="forms/cadastro/contrato.php?cd=<?php echo $empid; ?>" href="javascript:;"  class="novobotao text-center"><b>Novo Contrato</b></div>
			</div>
		</div>
		<div class="row">
			<div id="tabela" class="col-md-12">
				<table class="table table-dark table-responsive-xl table-sm" border="2">
					<thead class="thead-light">
						<tr class="text-center">
							<th class="text-center">Código do contrato</th>
							<th class="text-center">Nome da Empresa</th>
							<th class="text-center">CNAE</th>
							<th class="text-center">Grau de risco da empresa</th>
							<th class="text-center">Número</th>
							<th class="text-center">Responsável</th>
							<th class="text-center">Data Inicial da Validade do Programa Legal</th>
							<th class="text-center">Data Final da Validade do Programa Legal</th>
							<th class="text-center">Descrição do contrato</th>
							<th class="text-center">Valor</th>
							<th class="text-center">Data Inicial de Execução</th>
							<th class="text-center">Data Final de Execução</th>
							<th class="text-center">Equipe Envolvida</th>
							<th class="text-center">LTCAT</th>
							<th class="text-center">PPRA Quantitativo</th>
							<th class="text-center">PPRA Inserção Direta</th>
							<th class="text-center">PPRA Qualitativo</th>
							<th class="text-center">Gestão</th>
							<th class="text-center">Medições Ambientais</th>
							<th class="text-center">Resumo</th>
							<th class="text-center">Editar Contrato</th>
							<th class="text-center">Excluir Contrato</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if($res2->num_rows > 0){
							while ($row4 = mysqli_fetch_assoc($res4)){
								while($row2 = mysqli_fetch_assoc($res2)){
										if($row2["LTCAT"] == true){
											$LTCAT = "true.png";
										}else{
											$LTCAT = "false.png";
										}
										if($row2["PPRA_quant"] == true){
											$PPRAQN = "true.png";
										}else{
											$PPRAQN = "false.png";
										}
										if($row2["PPRA_direta"] == true){
											$PPRADI = "true.png";
										}else{
											$PPRADI = "false.png";
										}
										if($row2["PPRA_qualit"] == true){
											$PPRAQU = "true.png";
										}else{
											$PPRAQU = "false.png";
										}
										if($row2["gestao"] == true){
											$gestao = "true.png";
										}else{
											$gestao = "false.png";
										}
										if($row2["med_ambient"] == true){
											$medamb = "true.png";
										}else{
											$medamb = "false.png";
										}
										echo '
											<tr>
												<td class="text-center"><b>' . $row2["cdContrato"] . '</b></td>
												<td class="text-center"><b>' . $row4["nomeEmpresa"] . '</b></td>
												<td class="text-center"><b>' . $row2["CNAE"] . '</b></td>
												<td class="text-center"><b>' . $row2["risco_empresa"] . '</b></td>
												<td class="text-center"><b>' . $row2["unidade"] . '</b></td>
												<td class="text-center"><b>' . $row2["responsavelEmpresa"] . '</b></td>
												<td class="text-center"><b>' . $row2["v_data_inicial"] .  '</a></b></td>
												<td class="text-center"><b>' . $row2["v_data_final"] . '</b></td>
												<td class="text-center"><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/document.png" data-fancybox data-type="ajax" data-src="desc_contrato.php?cd=' . $row2["cdContrato"] . ' href="javascript:;"/></b></td>
												<td class="text-center"><b>' . $row2["valor"] . '</b></td>
												<td class="text-center"><b>' . $row2["exec_data_inicial"] . '</b></td>
												<td class="text-center"><b>' . $row2["exec_data_final"] . '</b></td>
												<td class="text-center"><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/document.png" data-fancybox data-type="ajax" data-src="contrato_equipe.php?cd=' . $row2["cdContrato"] . ' href="javascript:;"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $LTCAT . '"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $PPRAQN . '"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $PPRADI . '"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $PPRAQU . '"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $gestao . '"/></b></td>
												<td class="text-center"><b><img width="24px" height="24px" src="img/icons/' . $medamb . '"/></b></td>
												<td class="text-center"><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/document.png" data-fancybox data-type="ajax" data-src="contrato_resumo.php?cd=' . $row2["cdContrato"] . ' href="javascript:;"/></b></td>
												<td class="text-center"><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/edit.png" data-fancybox data-type="ajax" data-src="cadastro/edit_contrato.php?cd=' . $row2["cdContrato"] . ' href="javascript:;"/></b></td>
												<td class="text-center"><b><img class="icone2" style="cursor: pointer" width="24px" height="24px" src="img/icons/false.png" data-fancybox data-type="ajax" data-src="cadastro/del_contrato.php?cd=' . $row2["cdContrato"] . ' href="javascript:;"/></b></td>
											</tr>
										';
								}
							}
						}else{
							echo'
								<tr>
									<td colspan="20" class="text-center" ><b>Nenhum Contrato encontrado</b></td>
								</tr>
								';
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</html>