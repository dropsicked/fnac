<!DOCTYPE html>
<html lang="en">
<?php
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
	header ('Content-type: text/html; charset=UTF-8');
	include "../../php/connect.php";
	if (!isset($_SESSION)) session_start();
	if (!isset($_SESSION["cdLogin"])) {
		//Destrói a sessão por segurança
		session_destroy();
		//Redireciona o visitante de volta pro login
		header("Location: index.php"); exit;
	}
	if (!isset($_SESSION['cdcontrato'])){
		header ("Location: sel.php");
	}
	$cdLogin = $_SESSION["cdLogin"];
	$LNome = $_SESSION["nome"];
	$_SESSION['cd'] = $_SESSION['cdempresa'];
	$contrato = $_SESSION['cdcontrato'];
	$sql1 = "SELECT * FROM tbsubgrupo";
	$qry1 = mysqli_query($link,$sql1);
?>
	<head>
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title><?php echo $_SESSION["nome"]; ?> - Projeto FAR</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" href="../../css/fnac.css">
		
		<!-- Custom styles for this template -->
		<link href="../../css/scrolling-nav.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../../css/jquery.fancybox.min.css">
		<!-- Bootstrap core JavaScript -->
		<script src="../../js/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="../../js/bootstrap.min.js"></script>

		<!-- Plugin JavaScript -->
		<!--<script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->

		<!-- Custom JavaScript for this theme -->
		<script src="../../js/scrolling-nav.js"></script>
		<script src="../../js/jquery.fancybox.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="col-12">
				<form id="" class="" action="post/form_cadAgente.php" method="POST">
					<table class="table-sm table table-responsive-xl table-light">
						<thead class="thead-dark">
							<th class="text-center" colspan="2">Novo Agente</th>
						</thead>
						<tbody>
							<tr>
								<td>Grupo Agente
									<img src="/fnac/img/icons/edit.png" style="cursor: pointer" alt="Editar Grupos" data-fancybox data-type="ajax" data-src="forms/grupos.php" href="javascript:;" width="24px" height="24px"/>
								</td>
								<td><select id="" class="" name="tpAgente" disabled>
										<option id="" class="">Linkada ao Sub-Grupo</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Sub-Grupo(Família)
									<img src="/fnac/img/icons/edit.png" style="cursor: pointer" alt="Editar Sub-Grupos" data-fancybox data-type="ajax" data-src="forms/subgrupos.php" href="javascript:;" width="24px" height="24px"/>
								</td>
								<td>
								<?php
									if($qry2->num_rows > 0){
										echo '<select id="" class="" name="subGrupo">';
										while($row1 = mysqli_fetch_assoc($qry1)){
											$sql2 = "SELECT * FROM tbtipoagente WHERE cdTipoAgente = ".$row1["cdTipoAgente"];
											$qry2 = mysqli_query($link,$sql2);
											while ($row2 = mysqli_fetch_assoc($qry2)){
											echo '<option id="" class="">'.$row2["tipoAgente"].'-'.$row1["nome"].'</option>';
											}
										}
									}else{
										echo '
											<select id="" class="" name="subGrupo">
												<option id="" class="">Nenhum Sub Grupo cadastrado</option>
											';
									}
								?>
									</select><br>
								</td>
							</tr>
							<tr>
								<td>Nome do Agente</td>
								<td><input type="text" id="" class="" name="nmAgente" placeholder="Nome do Agente" required></td>
							</tr>
							<tr>
								<td>Código do Agente</td>
								<td><input type="text" id="" class="" name="codigoAgente" placeholder="Código do Agente" required></td>
							</tr>
							<tr>
								<td>Código E-Social</td>
								<td><input type="text" id="" class="" name="codigoESocial" placeholder="Código E-Social" required></td>
							</tr>
							<tr>
								<td>Unidade de Medida</td>
								<td><input type="text" id="" class="" name="unidadeMedida" placeholder="Unidade de Medida" required></td>
							</tr>
							<tr>
								<td>Divisor</td>
								<td><input type="text" id="" class="" name="divisor" placeholder="Divisor" required></td>
							</tr>
							<tr>
								<td>Constante</td>
								<td><input type="text" id="" class="" name="constante" placeholder="Constante" required></td>
							</tr>
							<tr>
								<td>Nível de Ação</td>
								<td><input type="text" id="" class="" name="nivelAcao" placeholder="Nível de Ação" required></td>
							</tr>
							<tr>
								<td>Limite de Exposição</td>
								<td><input type="text" id="" class="" name="limiteExposicao" placeholder="Limite de Exposição" required></td>
							</tr>
							<tr>
								<td>Limite de Exposição Máxima</td>
								<td><input type="text" id="" class="" name="limiteExposicaoMaxima" placeholder="Limite de Exposição Máxima" required></td>
							</tr>
							<tr>
								<td>Nível de Ação ACGIH</td>
								<td><input type="text" id="" class="" name="nivelAcaoACGIH" placeholder="Nível de Ação ACGIH" required></td>
							</tr>
							<tr>
								<td>Método de Medição</td>
								<td><input type="text" id="" class="" name="metodoMedicao" placeholder="Método de Medição" required></td>
							</tr>
							<tr>
								<td>Aparelhagem</td>
								<td><input type="text" id="" class="" name="aparelhagem" placeholder="Aparelhagem" required></td>
							</tr>
							<tr>
								<td>Meios de Propagação</td>
								<td><input type="text" id="" class="" name="meioPropagacao" placeholder="Meios de Propagação" required></td>
							</tr>
							<tr>
								<td>Danos à saúde</td>
								<td><textarea cols="40" rows="10" id="" class="" action="" name="danoSaude" value="Danos à saúde" required></textarea></td>
							</tr>
							<tr>				
								<td><input type="submit" id="" class="" name="" value="Confirmar Cadastro"></td>
								<td><input type="reset" id="" class="" name="" value="Limpar Campos"></td>
							</tr>
						</tbody>
					</table>
				</form>
			<div class="col-12">
		</div>
	</body>