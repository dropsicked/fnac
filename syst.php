<!DOCTYPE html>
<html lang="en">
<?php
 	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
	header ('Content-type: text/html; charset=UTF-8');
	include "php/connect.php";
	if (!isset($_SESSION)) session_start();
	if (!isset($_SESSION["cdLogin"])) {
		//Destrói a sessão por segurança
		session_destroy();
		//Redireciona o visitante de volta pro login
		header("Location: index.php"); exit;
	}
	if (!isset($_SESSION['cdcontrato'])){
		header ("Location: sel.php");
	}
	$cdLogin = $_SESSION["cdLogin"];
	$LNome = $_SESSION["nome"];
	$_SESSION['cd'] = $_SESSION['cdempresa'];
	$contrato = $_SESSION['cdcontrato'];
	print_r($_SESSION);
?>
	<head>
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title><?php echo $_SESSION["nome"]; ?> - Projeto FAR</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fnac.css">
		
		<!-- Custom styles for this template -->
		<link href="css/scrolling-nav.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
		<!-- Bootstrap core JavaScript -->
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="js/bootstrap.min.js"></script>

		<!-- Plugin JavaScript -->
		<!--<script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->

		<!-- Custom JavaScript for this theme -->
		<script src="js/scrolling-nav.js"></script>
		<script src="js/jquery.fancybox.min.js"></script>
		<script>
		$(document).ready(function(){
		    $("#clk2").click(function(){
				$("#base").load("forms/empresas.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#base").load("forms/empresas.php",function(){
				$("#base").fadeOut(0);
				$("#base").fadeIn(1000);
			});
			$("#clk3").click(function(){
				$("#base").load("forms/ghe.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#clk4").click(function(){
					$("#base").load("forms/agente.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#clk7").click(function(){
				$("#base").load("forms/epi.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#clk8").click(function(){
				$("#base").load("forms/equipamento.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#clk9").click(function(){
				$("#base").load("forms/users.php", function(){
					$("body").fadeIn(800);
				});
			});
			$("#sair").click(function(){
				$("body").fadeOut(800, function(){
					window.location = "php/logout.php";
				});
			});
		});
		/* Set the width of the side navigation to 250px */
		function openNav() {
			$("#base").toggle(500, function(){
				document.getElementById("mySidenav").style.width = "250px";
				$("#base").fadeOut(800);
				document.body.style.backgroundColor = "#ccc";	
			});
		}

		/* Set the width of the side navigation to 0 */
		function closeNav() {
			$("#base").toggle(500, function(){
				document.getElementById("mySidenav").style.width = "0";
				$("#base").fadeIn(800);
				document.body.style.backgroundColor = "white";
			});
		}
		</script>
	</head>
	<body>
		<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
			<div class="row">
				<div class="col-md-1">
					<button onclick="openNav()" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>
				<div class="col-md-10"></div>
				<div class="col-md-1"></div>
			</div>
		</nav>
		<div id="mySidenav" class="sidenav">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					<div onclick="closeNav()" id="clk1" class="icone">
						<img src="img/icons/empresas.png"/>
						<a class="nav-link text-center"><?php echo $_SESSION["nome"]; ?>
						</a>
					</div>
					<div onclick="closeNav()" id="clk2" class="icone">
						<img src="img/icons/empresas.png"/>
						<a class="nav-link text-center">Empresas</a>
					</div>
					<div onclick="closeNav()" id="clk3" class="icone">
						<img src="img/icons/agente.png"/>
						<a class="nav-link text-center">GHE</a>
					</div>
					<div onclick="closeNav()" id="clk4" class="icone">
						<img src="img/icons/agente.png"/>
						<a class="nav-link text-center">Agente</a>
					</div>
					<div onclick="closeNav()" id="clk5" class="icone">
						<img src="img/icons/agente.png"/>
						<a class="nav-link text-center">APR-HO</a>
					</div>
					<div onclick="closeNav()" id="clk6" class="icone">
						<img src="img/icons/agente.png"/>
						<a class="nav-link text-center">Ficha de Campo</a>
					</div>
					<div onclick="closeNav()" id="clk7" class="icone">
						<img src="img/icons/equip.png"/>
						<a class="nav-link text-center">EPI</a>
					</div>
					<div onclick="closeNav()" id="clk8" class="icone">
						<img src="img/icons/equip.png"/>
						<a class="nav-link text-center">Equipamento</a>
					</div>
					<div onclick="closeNav()" id="clk9" class="icone">
						<img src="img/icons/delete.png"/>
						<a class="nav-link text-center">Usuarios</a>
					</div>
					<div onclick="closeNav()" id="sair" class="icone">
						<img src="img/icons/delete.png"/>
						<a class="nav-link text-center">Sair</a>
					</div>
		</div>
		<div id="main" class="container-fluid">
			<div class="row text-center">
				<div class="col-md-12">
					<div class="base" id="base"></div>
				</div>
			</div>
		</div>
	    <!-- Footer -->
		<footer class="py-3 bg-dark fixed-bottom">
		  <div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; LCAT 2018</p>
		  </div>
		  <!-- /.container -->
		</footer>


	</body>
</html>