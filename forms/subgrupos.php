<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php 
 	error_reporting(E_ALL ^ E_DEPRECATED);
	include "../php/connect.php";
	$sql1 = "SELECT * From tbsubgrupo";
	$res1 = mysqli_query($connect,$sql1);
	header ('Content-type: text/html; charset=UTF-8');
?>
	<div class="container-fluid">
	<div class="row"><div class="col-12"><h3>Sub Grupos Agente</h3></div></div>
		<div class="row">
			<div class="col-6"></div>
			<div class="col-6">
				<div data-fancybox data-type="ajax" data-src="/fnac/forms/cadastro/grupos.php?cd=2" href="javascript:;" class="novobotao text-center" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Novo Sub Grupo</b></div>
			</div>
		</div>
		<div class="row">			
			<div id="tabela" class="col-md-12">
				<table class="table table-striped table-responsive-xl" border="2" style="margin-top: 40px">
					<thead class="thead-dark">
						<tr>
							<th>Código do Sub Grupo</th>
							<th>Grupo</th>
							<th>Sub Grupo</th>
							<th>Modificar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($res1->num_rows > 0){
								while($row = mysqli_fetch_assoc($res1)){
									$sql2 = "SELECT * From tbtipoagente WHERE cdTipoAgente = ".$row["cdTipoAgente"];
									$res2 = mysqli_query($connect,$sql2);
									while($row2 = mysqli_fetch_assoc($res2)){
										echo '
											<tr>
												<td class="text-center"><b>' . $row["cdSubGrupo"] . '</b></td>
												<td class="text-center"><b>' . $row2["tipoAgente"] . '</b></td>
												<td class="text-center"><b>' . $row["nome"] . '</b></td>
												<td class="text-center"><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/edit/edit_grupo.php?cd=' . $row["cdTipoAgente"] . '" href="javascript:;" class="icone2" style="cursor: pointer" width="24px" height="24px" src="/fnac/img/icons/edit.png"/></b></td>
												<td class="text-center"><b><img data-fancybox data-type="ajax" data-src="forms/cadastro/post/form_delGrupo.php?cd=' . $row["cdTipoAgente"] . '" href="javascript:; class="icone2" style="cursor: pointer" width="24px" height="24px" src="/fnac/img/icons/delete.png"/></b></td>
											</tr>
										';
									}
								}
							}
						?>
					<tbody>
				</table>
			</div>
		</div>
	</div>
</html>