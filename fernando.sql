-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08-Mar-2018 às 19:22
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `sistema_fernando`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbagente`
--

CREATE TABLE `tbagente` (
  `cdAgente` int(11) NOT NULL,
  `tipoAgente` text COLLATE utf8_unicode_ci NOT NULL,
  `subGrupo` text COLLATE utf8_unicode_ci NOT NULL,
  `nomeAgente` text COLLATE utf8_unicode_ci NOT NULL,
  `codigoAgente` text COLLATE utf8_unicode_ci NOT NULL,
  `codigoESocial` text COLLATE utf8_unicode_ci NOT NULL,
  `unidadeMedida` text COLLATE utf8_unicode_ci NOT NULL,
  `divisor` text COLLATE utf8_unicode_ci NOT NULL,
  `constante` text COLLATE utf8_unicode_ci NOT NULL,
  `nivelAcao` text COLLATE utf8_unicode_ci NOT NULL,
  `limiteExposicao` text COLLATE utf8_unicode_ci NOT NULL,
  `limiteExposicaoMaxima` text COLLATE utf8_unicode_ci NOT NULL,
  `nivelAcaoACGIH` text COLLATE utf8_unicode_ci NOT NULL,
  `metodoMedicao` text COLLATE utf8_unicode_ci NOT NULL,
  `aparelhagem` text COLLATE utf8_unicode_ci NOT NULL,
  `danoSaude` text COLLATE utf8_unicode_ci NOT NULL,
  `meioPropagacao` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbagente`
--

INSERT INTO `tbagente` (`cdAgente`, `tipoAgente`, `subGrupo`, `nomeAgente`, `codigoAgente`, `codigoESocial`, `unidadeMedida`, `divisor`, `constante`, `nivelAcao`, `limiteExposicao`, `limiteExposicaoMaxima`, `nivelAcaoACGIH`, `metodoMedicao`, `aparelhagem`, `danoSaude`, `meioPropagacao`) VALUES
(1, 'QuÃ­mico', 'Fumos MetÃ¡licos', 'Fluoreto', 'QFM0001', 'Fluoreto', 'M/SÂ¹,75, DBA, M/SÂ², PPM, MG/MÂ³(mG3)', 'Divisor', 'Constante', 'NivelAcao', 'LimiteExposiÃ§Ã£o', 'LimiteExposiÃ§Ã£oMaxima', 'NivelAcaoCGIH', 'MetodoMediÃ§Ã£o', 'Aparelhagem', '', 'Meios de PropagaÃ§Ã£o'),
(3, 'QuÃ­mico', 'Fumos NÃ£o-MetÃ¡licos', 'a', 'a', 'a', 'M/SÂ², PPM', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '', 'a');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbamostra`
--

CREATE TABLE `tbamostra` (
  `cdAmostra` int(11) NOT NULL,
  `amostra` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cdFichaQuim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbamostra_agente`
--

CREATE TABLE `tbamostra_agente` (
  `cdAgente` int(11) NOT NULL,
  `cdAmostra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbaprho`
--

CREATE TABLE `tbaprho` (
  `cdaprho` int(11) NOT NULL,
  `cdghe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbcontrato`
--

CREATE TABLE `tbcontrato` (
  `cdContrato` int(11) NOT NULL,
  `cdEmpresa` int(11) NOT NULL,
  `CNAE` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `risco_empresa` longtext COLLATE utf8_unicode_ci NOT NULL,
  `unidade` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `responsavelEmpresa` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `v_data_inicial` datetime NOT NULL,
  `v_data_final` datetime NOT NULL,
  `desc_contrato` longtext COLLATE utf8_unicode_ci NOT NULL,
  `valor` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exec_data_inicial` datetime NOT NULL,
  `exec_data_final` datetime NOT NULL,
  `equipe_envolv` longtext COLLATE utf8_unicode_ci NOT NULL,
  `LTCAT` tinyint(1) NOT NULL,
  `PPRA_quant` tinyint(1) NOT NULL,
  `PPRA_direta` tinyint(1) NOT NULL,
  `PPRA_qualit` tinyint(1) NOT NULL,
  `gestao` tinyint(1) NOT NULL,
  `med_ambient` tinyint(1) NOT NULL,
  `resumo` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbcontrato`
--

INSERT INTO `tbcontrato` (`cdContrato`, `cdEmpresa`, `CNAE`, `risco_empresa`, `unidade`, `responsavelEmpresa`, `v_data_inicial`, `v_data_final`, `desc_contrato`, `valor`, `exec_data_inicial`, `exec_data_final`, `equipe_envolv`, `LTCAT`, `PPRA_quant`, `PPRA_direta`, `PPRA_qualit`, `gestao`, `med_ambient`, `resumo`) VALUES
(0, 30, 'teste', 'teste', 'teste', 'teste', '2018-02-28 00:00:00', '2018-02-28 00:00:00', 'teste', 'teste', '2018-02-28 00:00:00', '2018-02-28 00:00:00', 'teste', 1, 1, 1, 1, 1, 1, 'teste'),
(2, 30, 'teste', 'teste', 'teste', 'teste', '2018-03-08 00:00:00', '2018-03-08 00:00:00', 'teste', 'teste', '2018-03-08 00:00:00', '2018-03-08 00:00:00', 'teste', 1, 1, 1, 1, 1, 1, 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbempresa`
--

CREATE TABLE `tbempresa` (
  `cdEmpresa` int(11) NOT NULL,
  `nomeEmpresa` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `endereco` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `razaoSocial` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `nomeFantasia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `nvEmpresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbempresa`
--

INSERT INTO `tbempresa` (`cdEmpresa`, `nomeEmpresa`, `endereco`, `razaoSocial`, `nomeFantasia`, `cnpj`, `arquivo`, `nvEmpresa`) VALUES
(30, 'FSFX', 'Rua jamil selim de sales, 896 apartamento 401\r\ncidade nova', 'Vale do Aço Soluções', 'FSFX', '77777777777777777', 'FSFX.png', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbepi`
--

CREATE TABLE `tbepi` (
  `cdEPI` int(11) NOT NULL,
  `tipoEPI` tinyint(4) NOT NULL COMMENT '(1) Capacete (2) Bota (3) Luva',
  `nome` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fabricante` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `modelo` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `ca` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `nivelProtecao` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbepi`
--

INSERT INTO `tbepi` (`cdEPI`, `tipoEPI`, `nome`, `fabricante`, `modelo`, `ca`, `nivelProtecao`) VALUES
(1, 1, 'teste', 'teste', 'teste', 'teste', 'teste'),
(2, 2, 'teste', 'teste', 'teste', 'teste', 'teste'),
(3, 3, 'teste', 'teste', 'teste', 'teste', 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbequipamento`
--

CREATE TABLE `tbequipamento` (
  `cdEquipamento` int(11) NOT NULL,
  `tipoEquipamento` tinyint(1) NOT NULL COMMENT '(0) Instrumento (1) Calibrador',
  `nome` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `numero` bigint(20) NOT NULL,
  `numeroSerie` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `certCalibracao` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dataValidade` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbequipamento`
--

INSERT INTO `tbequipamento` (`cdEquipamento`, `tipoEquipamento`, `nome`, `numero`, `numeroSerie`, `certCalibracao`, `dataValidade`) VALUES
(1, 1, 'teste 2 ', 21312312123, '123123123123', 'certificado 253.5', '2018-02-22 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbfichaquimic`
--

CREATE TABLE `tbfichaquimic` (
  `cdFichaQuim` int(11) NOT NULL,
  `codFicha` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cdGHE` int(11) NOT NULL,
  `dataAvaliacao` datetime NOT NULL,
  `cdInstrumento` int(11) NOT NULL,
  `cdCalibrador` int(11) NOT NULL,
  `cdAmostra` int(11) NOT NULL,
  `cdEPI` int(11) NOT NULL,
  `EPC` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `descAtividades` longtext COLLATE utf8_unicode_ci NOT NULL,
  `local` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `horaInicial` time NOT NULL,
  `horaFinal` time NOT NULL,
  `valHoraInicial` time NOT NULL,
  `valIntervaloInicial` time NOT NULL,
  `valIntervaloFinal` time NOT NULL,
  `valHoraFinal` time NOT NULL,
  `vazaoInicial` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `vazaoFinal` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `vazaoMedia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `varVazao` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `volumeTotal` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `temperatura` int(11) NOT NULL,
  `uraInicial` float NOT NULL,
  `uraFinal` float NOT NULL,
  `cdMetodo` int(11) NOT NULL,
  `diaTipico` tinyint(1) NOT NULL,
  `amostraValida` tinyint(1) NOT NULL,
  `colabAval` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricColabAval` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `supervImediato` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricSuperv` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `avaliador` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricAval` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbficharuido`
--

CREATE TABLE `tbficharuido` (
  `cdFichaRuido` int(11) NOT NULL,
  `codFicha` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cdGHE` int(11) NOT NULL,
  `dataAvaliacao` datetime NOT NULL,
  `cdInstrumento` int(11) NOT NULL,
  `cdCalibrador` int(11) NOT NULL,
  `cdEPI` int(11) NOT NULL,
  `EPC` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `descAtividades` longtext COLLATE utf8_unicode_ci NOT NULL,
  `local` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `horaInicial` time NOT NULL,
  `horaFinal` time NOT NULL,
  `valHoraInicial` time NOT NULL,
  `valIntervaloInicial` time NOT NULL,
  `valIntervaloFinal` time NOT NULL,
  `valHoraFinal` time NOT NULL,
  `diaTipico` tinyint(1) NOT NULL,
  `amostraValida` tinyint(1) NOT NULL,
  `colabAval` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricColabAval` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `supervImediato` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricSuperv` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `avaliador` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `matricAval` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbfotos`
--

CREATE TABLE `tbfotos` (
  `cdGHE` int(11) NOT NULL,
  `foto1` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `foto2` mediumtext COLLATE utf8_unicode_ci,
  `foto3` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbgerencia`
--

CREATE TABLE `tbgerencia` (
  `cdGerencia` int(11) NOT NULL,
  `cdEmpresa` int(11) NOT NULL,
  `cdSuperIntendencia` int(11) NOT NULL,
  `gerencia` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbgerencia`
--

INSERT INTO `tbgerencia` (`cdGerencia`, `cdEmpresa`, `cdSuperIntendencia`, `gerencia`) VALUES
(0, 30, 0, 'GerÃªncia Laudos MÃ©dicos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbghe`
--

CREATE TABLE `tbghe` (
  `cdGHE` int(11) NOT NULL,
  `cdContrato` int(11) NOT NULL,
  `cdSetor` int(11) NOT NULL,
  `codGHE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nomeGHE` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `numEmpregados` int(11) NOT NULL,
  `jornadaTrab` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descTrab` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tipoCont` int(11) NOT NULL COMMENT '(1) LTCAT (2) PPRA Quantitativo (3) PPRA Inserção Direta (4) PPRA Qualitativo',
  `descAtiv` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cargosGHE` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tblogin`
--

CREATE TABLE `tblogin` (
  `cdLogin` int(11) NOT NULL,
  `nome_real` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `telefone` int(11) NOT NULL,
  `endereco` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `data_cad` datetime NOT NULL,
  `p_read` tinyint(1) NOT NULL,
  `p_write` tinyint(1) NOT NULL,
  `p_caduser` tinyint(1) NOT NULL,
  `p_admin` tinyint(1) NOT NULL,
  `nvUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tblogin`
--

INSERT INTO `tblogin` (`cdLogin`, `nome_real`, `user`, `pass`, `email`, `telefone`, `endereco`, `data_cad`, `p_read`, `p_write`, `p_caduser`, `p_admin`, `nvUsuario`) VALUES
(1, 'teste1 teste2 teste3 teste4', 'teste', '2e6f9b0d5885b6010f9167787445617f553a735f', 'teste', 124345345, 'teaste', '2018-02-15 00:00:00', 1, 1, 1, 1, 10),
(2, 'Fernando Ribeiro', 'fernandoribeiro', '932169c217f174501391e7b54325aa2ea8f6c95f', 'teste@teste.com', 70707070, 'Rua teste,105,Bom Retiro', '2018-02-28 00:00:00', 1, 1, 1, 1, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbrisco`
--

CREATE TABLE `tbrisco` (
  `cdrisco` int(11) NOT NULL,
  `cdAgente` int(11) NOT NULL,
  `fonte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `controle` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `exposicao` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbsetor`
--

CREATE TABLE `tbsetor` (
  `cdSetor` int(11) NOT NULL,
  `cdEmpresa` int(11) NOT NULL,
  `cdGerencia` int(11) NOT NULL,
  `setor` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbsetor`
--

INSERT INTO `tbsetor` (`cdSetor`, `cdEmpresa`, `cdGerencia`, `setor`) VALUES
(0, 30, 0, 'Setor de Laudos Urgentes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbsuperintendencia`
--

CREATE TABLE `tbsuperintendencia` (
  `cdSuperIntendencia` int(11) NOT NULL,
  `cdEmpresa` int(11) NOT NULL,
  `superintendencia` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tbsuperintendencia`
--

INSERT INTO `tbsuperintendencia` (`cdSuperIntendencia`, `cdEmpresa`, `superintendencia`) VALUES
(0, 30, 'Super IntendÃªncia Ipatinga');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbagente`
--
ALTER TABLE `tbagente`
  ADD PRIMARY KEY (`cdAgente`);

--
-- Indexes for table `tbamostra`
--
ALTER TABLE `tbamostra`
  ADD PRIMARY KEY (`cdAmostra`),
  ADD KEY `cdFichaQuim` (`cdFichaQuim`);

--
-- Indexes for table `tbamostra_agente`
--
ALTER TABLE `tbamostra_agente`
  ADD KEY `cdAgente` (`cdAgente`),
  ADD KEY `cdFichaQuimic` (`cdAmostra`);

--
-- Indexes for table `tbaprho`
--
ALTER TABLE `tbaprho`
  ADD PRIMARY KEY (`cdaprho`),
  ADD KEY `cdghe` (`cdghe`);

--
-- Indexes for table `tbcontrato`
--
ALTER TABLE `tbcontrato`
  ADD PRIMARY KEY (`cdContrato`),
  ADD KEY `cdEmpresa` (`cdEmpresa`);

--
-- Indexes for table `tbempresa`
--
ALTER TABLE `tbempresa`
  ADD PRIMARY KEY (`cdEmpresa`);

--
-- Indexes for table `tbepi`
--
ALTER TABLE `tbepi`
  ADD PRIMARY KEY (`cdEPI`);

--
-- Indexes for table `tbequipamento`
--
ALTER TABLE `tbequipamento`
  ADD PRIMARY KEY (`cdEquipamento`);

--
-- Indexes for table `tbfichaquimic`
--
ALTER TABLE `tbfichaquimic`
  ADD PRIMARY KEY (`cdFichaQuim`),
  ADD UNIQUE KEY `codFicha` (`codFicha`),
  ADD KEY `cdGHE` (`cdGHE`),
  ADD KEY `cdCalibrador` (`cdCalibrador`),
  ADD KEY `cdInstrumento` (`cdInstrumento`),
  ADD KEY `cdEPI` (`cdEPI`),
  ADD KEY `cdAmostra` (`cdAmostra`);

--
-- Indexes for table `tbficharuido`
--
ALTER TABLE `tbficharuido`
  ADD PRIMARY KEY (`cdFichaRuido`),
  ADD UNIQUE KEY `codFicha` (`codFicha`),
  ADD KEY `cdGHE` (`cdGHE`),
  ADD KEY `cdInstrumento` (`cdInstrumento`),
  ADD KEY `cdCalibrador` (`cdCalibrador`),
  ADD KEY `cdEPI` (`cdEPI`);

--
-- Indexes for table `tbfotos`
--
ALTER TABLE `tbfotos`
  ADD KEY `cdGHE` (`cdGHE`);

--
-- Indexes for table `tbgerencia`
--
ALTER TABLE `tbgerencia`
  ADD PRIMARY KEY (`cdGerencia`),
  ADD KEY `cdSuperIntendencia` (`cdSuperIntendencia`),
  ADD KEY `cdEmpresa` (`cdEmpresa`);

--
-- Indexes for table `tbghe`
--
ALTER TABLE `tbghe`
  ADD PRIMARY KEY (`cdGHE`),
  ADD UNIQUE KEY `codGHE` (`codGHE`),
  ADD KEY `cdContrato` (`cdContrato`),
  ADD KEY `cdSetor` (`cdSetor`);

--
-- Indexes for table `tblogin`
--
ALTER TABLE `tblogin`
  ADD PRIMARY KEY (`cdLogin`);

--
-- Indexes for table `tbrisco`
--
ALTER TABLE `tbrisco`
  ADD PRIMARY KEY (`cdrisco`);

--
-- Indexes for table `tbsetor`
--
ALTER TABLE `tbsetor`
  ADD PRIMARY KEY (`cdSetor`),
  ADD KEY `cdGerencia` (`cdGerencia`),
  ADD KEY `cdEmpresa` (`cdEmpresa`);

--
-- Indexes for table `tbsuperintendencia`
--
ALTER TABLE `tbsuperintendencia`
  ADD PRIMARY KEY (`cdSuperIntendencia`),
  ADD KEY `cdEmpresa` (`cdEmpresa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbagente`
--
ALTER TABLE `tbagente`
  MODIFY `cdAgente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbamostra`
--
ALTER TABLE `tbamostra`
  MODIFY `cdAmostra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbaprho`
--
ALTER TABLE `tbaprho`
  MODIFY `cdaprho` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbcontrato`
--
ALTER TABLE `tbcontrato`
  MODIFY `cdContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbempresa`
--
ALTER TABLE `tbempresa`
  MODIFY `cdEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbepi`
--
ALTER TABLE `tbepi`
  MODIFY `cdEPI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbequipamento`
--
ALTER TABLE `tbequipamento`
  MODIFY `cdEquipamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbfichaquimic`
--
ALTER TABLE `tbfichaquimic`
  MODIFY `cdFichaQuim` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbficharuido`
--
ALTER TABLE `tbficharuido`
  MODIFY `cdFichaRuido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbgerencia`
--
ALTER TABLE `tbgerencia`
  MODIFY `cdGerencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbghe`
--
ALTER TABLE `tbghe`
  MODIFY `cdGHE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblogin`
--
ALTER TABLE `tblogin`
  MODIFY `cdLogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbrisco`
--
ALTER TABLE `tbrisco`
  MODIFY `cdrisco` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbsetor`
--
ALTER TABLE `tbsetor`
  MODIFY `cdSetor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbsuperintendencia`
--
ALTER TABLE `tbsuperintendencia`
  MODIFY `cdSuperIntendencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbamostra_agente`
--
ALTER TABLE `tbamostra_agente`
  ADD CONSTRAINT `tbamostra_agente_ibfk_1` FOREIGN KEY (`cdAmostra`) REFERENCES `tbamostra` (`cdAmostra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbamostra_agente_ibfk_2` FOREIGN KEY (`cdAgente`) REFERENCES `tbagente` (`cdAgente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbcontrato`
--
ALTER TABLE `tbcontrato`
  ADD CONSTRAINT `tbcontrato_ibfk_1` FOREIGN KEY (`cdEmpresa`) REFERENCES `tbempresa` (`cdEmpresa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbfichaquimic`
--
ALTER TABLE `tbfichaquimic`
  ADD CONSTRAINT `tbfichaquimic_ibfk_1` FOREIGN KEY (`cdGHE`) REFERENCES `tbghe` (`cdGHE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbfichaquimic_ibfk_5` FOREIGN KEY (`cdAmostra`) REFERENCES `tbamostra` (`cdAmostra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbfichaquimic_ibfk_6` FOREIGN KEY (`cdEPI`) REFERENCES `tbepi` (`cdEPI`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbfichaquimic_ibfk_7` FOREIGN KEY (`cdCalibrador`) REFERENCES `tbequipamento` (`cdEquipamento`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbfichaquimic_ibfk_8` FOREIGN KEY (`cdInstrumento`) REFERENCES `tbequipamento` (`cdEquipamento`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbficharuido`
--
ALTER TABLE `tbficharuido`
  ADD CONSTRAINT `tbficharuido_ibfk_1` FOREIGN KEY (`cdGHE`) REFERENCES `tbghe` (`cdGHE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbficharuido_ibfk_4` FOREIGN KEY (`cdEPI`) REFERENCES `tbepi` (`cdEPI`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbficharuido_ibfk_5` FOREIGN KEY (`cdCalibrador`) REFERENCES `tbequipamento` (`cdEquipamento`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbficharuido_ibfk_6` FOREIGN KEY (`cdInstrumento`) REFERENCES `tbequipamento` (`cdEquipamento`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbfotos`
--
ALTER TABLE `tbfotos`
  ADD CONSTRAINT `tbfotos_ibfk_1` FOREIGN KEY (`cdGHE`) REFERENCES `tbghe` (`cdGHE`);

--
-- Limitadores para a tabela `tbgerencia`
--
ALTER TABLE `tbgerencia`
  ADD CONSTRAINT `tbgerencia_ibfk_1` FOREIGN KEY (`cdSuperIntendencia`) REFERENCES `tbsuperintendencia` (`cdSuperIntendencia`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbgerencia_ibfk_2` FOREIGN KEY (`cdEmpresa`) REFERENCES `tbempresa` (`cdEmpresa`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbghe`
--
ALTER TABLE `tbghe`
  ADD CONSTRAINT `tbghe_ibfk_1` FOREIGN KEY (`cdContrato`) REFERENCES `tbcontrato` (`cdContrato`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbghe_ibfk_2` FOREIGN KEY (`cdSetor`) REFERENCES `tbsetor` (`cdSetor`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbsetor`
--
ALTER TABLE `tbsetor`
  ADD CONSTRAINT `tbsetor_ibfk_1` FOREIGN KEY (`cdGerencia`) REFERENCES `tbgerencia` (`cdGerencia`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbsetor_ibfk_2` FOREIGN KEY (`cdEmpresa`) REFERENCES `tbempresa` (`cdEmpresa`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbsuperintendencia`
--
ALTER TABLE `tbsuperintendencia`
  ADD CONSTRAINT `tbsuperintendencia_ibfk_1` FOREIGN KEY (`cdEmpresa`) REFERENCES `tbempresa` (`cdEmpresa`) ON UPDATE CASCADE;
COMMIT;
