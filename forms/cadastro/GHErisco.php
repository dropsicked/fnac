<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
	header ('Content-type: text/html; charset=UTF-8');
	include "php/connect.php";
	if (!isset($_SESSION)) session_start();
	if (!isset($_SESSION["cdLogin"])) {
		//Destrói a sessão por segurança
		session_destroy();
		//Redireciona o visitante de volta pro login
		header("Location: index.php"); exit;
	}
	if (!isset($_SESSION['cdcontrato'])){
		header ("Location: sel.php");
	}
	$cdLogin = $_SESSION["cdLogin"];
	$LNome = $_SESSION["nome"];
	$_SESSION['cd'] = $_SESSION['cdempresa'];
	$contrato = $_SESSION['cdcontrato'];
	print_r($_SESSION);
	$sql1 = "SELECT * FROM tbagente";
	$query1 = mysqli_query($link,$sql1);
	$cdghe = $_GET["cdghe"];
?>
	<head>
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title><?php echo $_SESSION["nome"]; ?> - Projeto FAR</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fnac.css">
		
		<!-- Custom styles for this template -->
		<link href="css/scrolling-nav.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
		<!-- Bootstrap core JavaScript -->
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="js/bootstrap.min.js"></script>

		<!-- Plugin JavaScript -->
		<!--<script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->

		<!-- Custom JavaScript for this theme -->
		<script src="js/scrolling-nav.js"></script>
		<script src="js/jquery.fancybox.min.js"></script>
		<script>
		$(document).ready(function(){
				
		}    
		</script>
	</head>
	<body>
		<form id="" class="" action="#" method="">
			<table class="table table-light table-stripped table-sm">
				<thead class="thead-dark">
					<tr>
						<th colspan="2">Novo Risco</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Tipo de Agente</td>
						<td>
							<select id="" class="" name="" disabled>
								<option id="" class="" value="" selected>Linkada ao Agente</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Sub-Grupo(Família)</td>
						<td>
							<select id="" class="" name=""disabled>
								<option id="" class="" value="" selected>Linkada ao Agente</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Nome do Agente</td>
						<td>
							<select id="" class="" name="">
								<?php
								while($row1 = mysqli_fetch_assoc($query1)){
									echo'<option id="" class="" value="'.$row1["cdAgente"].'">'.$row1["nomeAgente"].'</option>';
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Principais Fontes Geradoras:</td>
						<td><textarea id="" class="" name="" cols="30" rows="10"></textarea></td>
					</tr>
					<tr>
						<td>Medidas de Controle Existente</td>
						<td><textarea id="" class="" name="" cols="30" rows="10"></textarea></td>
					</tr>
					<tr>
						<td>Caracterização de Exposição</td>
						<td><input type="text" id="" class="" name="expo"></td>
					</tr>
					<tr>
						<td><input type="submit" id="" class="" name="" value="Confirmar Cadastro"></td>
						<td><input type="reset" id="" class="" name="" value="Limpar Campos"></td>
					</tr>
				</tbody>
			</table>
		</form>
	</body>
</html>