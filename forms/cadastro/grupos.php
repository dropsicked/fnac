<?php
include "../../php/connect.php";
$cd = $_GET["cd"];
if($cd == 1){
	echo '
	<div class="container">
		<table class="table table-sm table-responsive-xl table-light">
			<form id="" class="" action="/fnac/forms/cadastro/post/form_cadGrupos.php" method="POST">
				<thead class="thead-dark">
					<tr>
						<th colspan="2" class="text-center">Novo Grupo</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Nome do Grupo</td>
						<td><input type="text" name="nome"></td>
					</tr>
					<tr>				
						<td><input type="submit" id="" class="" name="btnSave" value="Confirmar Cadastro"></td>
						<td><input type="reset" id="" class="" name="" value="Limpar Campos"></td>
					</tr>
				</tbody>
			</form>
		</table>
	</div>
	';
}else{
	$sql1 = "SELECT * FROM tbtipoagente";
	$qry1 = mysqli_query($link,$sql1);
	echo '
	<div class="container">
		<table class="table table-sm table-responsive-xl table-light">
			<form action="post/form_cadSubGrupos.php" method="post">
				<thead class="thead-dark">
					<tr>
						<th colspan="2" class="text-center">Novo Sub Grupo</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Grupo</td>
						<td>
							<select name="grupo">';
							while($row = mysqli_fetch_assoc($qry1)){
								echo'<option value="'.$row["cdTipoAgente"].'">'.$row["tipoAgente"].'</option>';
							}
							echo '</select>
						</td>
					</tr>
					<tr>
						<td>Nome do Sub Grupo</td>
						<td><input type="text" name="nome2"></td>
					</tr>
					<tr>				
						<td><input type="submit" id="" class="" name="btnSave" value="Confirmar Cadastro"></td>
						<td><input type="reset" id="" class="" name="" value="Limpar Campos"></td>
					</tr>
				</tbody>
			</form>
		</table>
	</div>
	';
}