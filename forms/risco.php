<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<?php 
 	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	include "../php/connect.php";
	session_start();
	$empid = $_SESSION["cdempresa"];
	$contrato = $_SESSION["cdcontrato"];
	$cdghe = $_GET["cdghe"];
	$sql1 = "SELECT * From tbgherisco WHERE cdGHE = ".$cdGHE;
	$res1 = mysqli_query($connect,$sql1);
	header ('Content-type: text/html; charset=UTF-8');
?>
	<div class="container-fluid">
		<div class="row"><div class="col-12"><h3>GHE</h3></div></div>
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8"></div>
			<div class="col-2">
				<a href="forms/cadastro/GHErisco.php?cd=<?php echo $empid;?>"><div  class="novobotao text-center" style="background-color: #94f441; cursor: pointer; color: white; border-radius: 2px;"><b>Nova GHE</b></div></a>
			</div>
		</div>
		<div class="row">
			<div id="tabela" class="col-md-12">
				<table class="table table-striped table-responsive-xl table-sm" border="2" style="margin-top: 40px">
					<thead class="thead-dark">
						<tr>
							<th>Código</th>							
							<th>Agente</th>
							<th>Fonte</th>
							<th>Controle</th>
							<th>Exposição</th>
							<th>Modificar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($res1->num_rows > 0){
								while($row1 = mysqli_fetch_assoc($res1)){
									$sql2 = "SELECT * FROM tbghe WHERE cdGHE = ".$row1["cdGHE"];
									$res2 = mysqli_query($link,$sql2);
									while($row2 = mysqli_fetch_assoc($res2)){
										$sql3 = "SELECT * From tbagente WHERE cdAgente = " . $row1["cdAgente"];
										$res3 = mysqli_query($connect,$sql3);
										while($row3 = mysqli_fetch_assoc($res3)){
											echo '
												<tr style="background-color: ">
													<td><b>' . $row1["cdRisco"] . '</b></td>
													<td><b>' . $row3["tipoAgente"] . '-' . $row3["subGrupo"] . '-' . $row3["nomeAgente"] . '</b></td>
													<td><b>' . $row1["fonte"] . '</b></td>
													<td><b>' . $row1["controle"] . '</b></td>
													<td><b>' . $row1["exposicao"] . '</b></td>
													<td><b><img src="img/icons/edit.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/cadastro/edit/ghe.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
													<td><b><img src="img/icons/delete.png" width="24px" height="24px" data-fancybox data-type="ajax" data-src="forms/cadastro/post/forms_delGHE.php?cd=' . $row["cdGHE"] . '" href="javascript:;" class="icone2"/></td>
												</tr>
											';
										}
								}
							}else{
								echo '
									<tr style="background-color: ">
										<td colspan="12">Nehnuma GHE Cadastrada</td>
									</tr>
								';
							}
						?>
					<tbody>
				</table>
			</div>
		</div>
	</div>
</html>